---
layout: page
title: About Sean
permalink: /about/
---

Sean is a Front-End Developer currently residing in Louisville, KY.
In his free time, he enjoys playing video games and strumming on his guitar or tapping on his piano.
He has a Beagle/Basset Hound mix named Roscoe that he loves _very_ much.
