---
layout: page
title: Colophon
permalink: /colophon/
---

### Software Used:

#### Text Editor
Visual Studio Code

#### Site Generator
Jekyll

#### Operating Systems
macOS Sierra, Ubuntu 16.10, and Windows 10.

Visual Studio Code: [https://code.visualstudio.com](https://code.visualstudio.com)  
Jekyll: [https://jekyllrb.com](https://jekyllrb.com)  
Ubuntu: [https://www.ubuntu.com](https://www.ubuntu.com)  
Windows: [https://www.microsoft.com/en-us/windows](https://www.microsoft.com/en-us/windows)  
macOS: [https://www.apple.com/macos/sierra](https://www.apple.com/macos/sierra)

Site Source Code: [https://gitlab.com/seanclayton/seanclayton.me](https://gitlab.com/seanclayton/seanclayton.me)