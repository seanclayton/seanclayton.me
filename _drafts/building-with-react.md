---
layout: post
title: Building With React
---

Over the past ~2 years I've been working with React full-time at work.
We faced many challenges along the way that I had to resolve:

1. We had a monolothic, 18-year-old Java application with no API (public _or_ private). How can we integrate with it?
1. We had to do partial view by partial view. We could not have React control the _entire_ page until everything was 100% rewritten in React.
1. Can't destructively hijack the URL bar.
1. Our application is a completely white-labelled application. Nearly every color, button, etc had to be customizable per client.
1. Our application is WCAG 2.0 compliant. We had to maintain that.
1. Our application is internationalized to a few languages (Canadian French, Frenchy French, and Spanish). We had to maintain those and be able to add more.
